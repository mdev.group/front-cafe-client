import moment from "moment";

export const roundToNearestXXMinutes = (start: any, roundTo: any) => {
  let remainder = roundTo - (start.minute()+ start.second()/60) % roundTo;

  remainder = (remainder >  roundTo/2) ? remainder = -roundTo + remainder : remainder;
  return moment(start).add(remainder, "minutes" ).seconds(0).format("HHmm");    
}