export const categories = [
  {
    label: 'Бургеры',
    icon: '/images/Food/burger.png',
    color: '#EF7272'
  },
  {
    label: 'Кофе',
    icon: '/images/Food/coffee.png',
    color: '#956B54'
  },
  {
    label: 'Суши',
    icon: '/images/Food/sushi.png',
    color: '#9ED0DF'
  },
  {
    label: 'Пицца',
    icon: '/images/Food/pizza.png',
    color: '#E1A27E'
  },
  {
    label: 'Выпечка',
    icon: '/images/Food/cake.png',
    color: '#DAB0E1'
  },
  {
    label: 'Вок',
    icon: '/images/Food/wok.png',
    color: '#DCA99E'
  },
  {
    label: 'Сладости',
    icon: '/images/Food/donut.png',
    color: '#DF9EBD'
  }
]

export const badges = [
  {
    label: 'На вынос',
    key: 'out',
    icon: '/images/badges/out.svg'
  },
  {
    label: 'Wi-Fi',
    key: 'wifi',
    icon: '/images/badges/wifi.svg'
  },
  {
    label: 'Зарядки',
    key: 'charge',
    icon: '/images/badges/charge.svg'
  },
  {
    label: 'Алкоголь',
    key: 'alco',
    icon: '/images/badges/alco.svg'
  }
]