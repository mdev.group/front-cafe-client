import { badges } from "core/constants/categories";
import { beginCafeListLoad } from "core/redux/actions/cafe";
import { SendOrder, updateOrder } from "core/redux/actions/order";
import { cafeByID_selector, cafes_selector, orderCartTotal_selector, ordersListTotals_selector, ordersList_selector, order_selector, user_selector } from "core/redux/selectors";
import { IItemDTO } from "core/redux/types/dto/item";
import { EOrderStatus } from "core/redux/types/dto/order";
import Appbar from "core/uikit/Appbar/Appbar";
import Badge from "core/uikit/Badge";
import Button from "core/uikit/Button";
import Dropdown from "core/uikit/Dropdown";
import Image from "core/uikit/Image";
import ItemCard from "core/uikit/ItemCard";
import ModalContainer from "core/uikit/ModalContainer";
import PageHeader from "core/uikit/PageHeader";
import PositionCard from "core/uikit/PositionCard";
import { isChild } from "core/utils/isChild";
import { mapStatusToString } from "core/utils/mapStatusToString";
import { roundToNearestXXMinutes } from "core/utils/roundToNearestXXMinutes";
import moment from "moment";
import type { NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
const TimeSelect = require('react-time-select');
import styles from 'styles/pages/CafePage.module.scss'

const Home: NextPage = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { orderID } = router.query;
  const user = useSelector(user_selector);
  const orders = useSelector(ordersList_selector);
  const totals = useSelector(ordersListTotals_selector);
  const cafes = useSelector(cafes_selector);

  const order = useMemo(() => orders.find((it) => it._id === orderID), [orderID, orders])
  const cafe = useMemo(() => cafes.find((it) => it._id === order?.cafeID), [order, cafes])
  const total = useMemo(() => totals?.find((it) => it.orderID === order?._id), [order, totals])

  useEffect(() => {
    dispatch(beginCafeListLoad({
      category: null,
      ids: orders.reduce((prev, ord) => {
        return [...prev, ord.cafeID]
      }, [] as string[])
    }));
  }, [orders, dispatch])


  return (
    <div>
      <main className={styles.cafePage}>
        <Appbar />
        <PageHeader
          title={`Заказ ${order?.number}`}
          backUrl={cafe ? '/orders' : '/'}
        />

        {cafe && order && total && (
          <>
            <div className={styles.cafePage__imageWrapper}>
              <Image
                className={styles.cafePage__image}
                src={cafe.photo_url || '/images/icons/blank/cafe.svg'}
                alt=""
              />
              <div className={styles.cafePage__badges}>
                <div>
                  {cafe.badges.map((badge, index) => (
                    <Badge
                      key={index}
                      className={styles.cafePage__badge}
                      title={badges.find((item) => item.key == badge)?.label || ''}
                    >
                      <Image
                        src={badges.find((item) => item.key == badge)?.icon || ''}
                        alt=""
                      />
                    </Badge>
                  ))}
                </div>
              </div>
            </div>

            <div className={styles.cafePage__container}>
              <h2>Заказ #{order.number}</h2>

              <div className={styles.cafePage__badges}>
                <Badge>
                  {mapStatusToString(order.status)}
                </Badge>
              </div>

              <div className={styles.cafePage__cartInfoWrapper}>

                <div
                  className={styles.cafePage__cartInfo}
                  onClick={() => {
                    window.open(`https://yandex.ru/maps/?pt=${cafe.location?.lng},${cafe.location?.lat}&z=18&l=map`, "_blank");
                  }}
                >
                  <span className={styles.cafePage__cartInfoLabel}>По адрессу</span>
                  <span className={styles.cafePage__cartInfoValue}>{cafe.address}</span>
                </div>

                <div className={styles.cafePage__cartInfo}>
                  <span className={styles.cafePage__cartInfoLabel}>Способ получения</span>
                  <span className={styles.cafePage__cartInfoValue}>
                    {order.table === null ? 'С собой' : 'В заведении'}
                    <Image
                      src="/images/icons/chevron-blue.svg"
                      alt=""
                    />
                  </span>
                </div>

                <div className={styles.cafePage__cartInfo}>
                  <span className={styles.cafePage__cartInfoLabel}>Время получения</span>
                  <span className={styles.cafePage__cartInfoValue}>
                    Как можно скорее
                  </span>
                </div>

                <div className={styles.cafePage__cartInfo}>
                  <span className={styles.cafePage__cartInfoLabel}>Оплата</span>
                  <span className={styles.cafePage__cartInfoValue}>
                    По факту получения
                  </span>
                </div>
              </div>

              <div className={styles.cafePage__cartItems}>
                {order.positions.map((pos) => (
                  <PositionCard
                    key={pos.uuid}
                    position={pos}
                    order={order}
                  />
                ))}
              </div>


              <div className={styles.cafePage__cartTotal}>
                <span>Итог:</span>
                <span>{total?.cost} руб.</span>
              </div>

            </div>
          </>
        )}

      </main>
    </div>
  );
};

export default Home;
