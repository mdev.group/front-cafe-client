import { takeEvery, put, call } from 'redux-saga/effects'
import api from '../../api';

import {
  getUserData as getUserDataAction, setOwnedCafes, setUserData, updateUser,
} from '../actions/user';
import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import type { IUserDTO } from '../types/dto/user';
import { Action } from 'redux-actions';
import { IClient } from '../types/dto/client';

function* getUserData(): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.getUserData)) as IClient;
    yield put(setUserData(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

function* update({payload}: Action<any>): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.updateUser, payload)) as IClient;
    yield put(setUserData(response));
  } catch (error) {
      yield put(requestError(error));
  }
}


export function* initUserWatchers(){
  yield takeEvery(getUserDataAction.toString(), getUserData);
  yield takeEvery(updateUser.toString(), update);
}