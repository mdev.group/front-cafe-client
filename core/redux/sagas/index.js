import { call, spawn, all } from 'redux-saga/effects'

import { initAuthWatchers } from './authSaga';
import { initCafeWatchers } from './cafeSaga';
import { initOrderWatchers } from './orderSaga';
import { initUserWatchers } from './userSaga';

export default function* rootSaga(){
  const sagas = [initAuthWatchers, initUserWatchers, initCafeWatchers, initOrderWatchers];
  
  const retrySagas = sagas.map(saga=>{
    return spawn(function* (){
      while(true){
        try{
          yield call(saga);
          break;
        } catch (e) {
          console.error(e)
        }
      }
    })
  })

  yield all(retrySagas)
}