import { beginCafeListLoad } from "core/redux/actions/cafe";
import { cafes_selector } from "core/redux/selectors";
import Appbar from "core/uikit/Appbar/Appbar";
import CafeCard from "core/uikit/CafeCard";
import PageHeader from "core/uikit/PageHeader";
import redirectTo from "core/utils/redirectTo";
import type { NextPage } from "next";
import Head from "next/head";
import { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from 'styles/pages/CatalogPage.module.scss'

const Home: NextPage = () => {
  const dispatch = useDispatch();
  const [favoriteIDS, setFavorites] = useState<any>([]);

  const cafes = useSelector(cafes_selector);

  useEffect(() => {
    const favorsJSON = window.localStorage.getItem('favorites') || '[]';
    if(favorsJSON) {
      const favors = JSON.parse(favorsJSON);
      console.log(favors);
      setFavorites(favors)

      dispatch(beginCafeListLoad({
        ids: favors,
        category: null
      }))
    }
  }, [dispatch])

  const filteredCafes = useMemo(() => cafes.filter((cafe) => favoriteIDS.includes(cafe._id)), [favoriteIDS, cafes]);

  return (
    <div>
      <main>
        <Appbar />
        <PageHeader title="Личный кабинет" />

        <div className={styles.page__cafes}>
          {filteredCafes.map((cafe) => (
            <CafeCard
              key={cafe._id}
              data={cafe}
              onClick={() => redirectTo(`/cafe/${cafe.code}`)}
            />
          ))}
        </div>
      </main>
    </div>
  );
};

export default Home;
