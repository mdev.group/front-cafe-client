import { badges } from "core/constants/categories";
import { beginCafeListLoad } from "core/redux/actions/cafe";
import { SendOrder, updateOrder } from "core/redux/actions/order";
import { cafeByID_selector, orderCartTotal_selector, order_selector, user_selector } from "core/redux/selectors";
import { IItemDTO } from "core/redux/types/dto/item";
import { EOrderStatus } from "core/redux/types/dto/order";
import Appbar from "core/uikit/Appbar/Appbar";
import Badge from "core/uikit/Badge";
import Button from "core/uikit/Button";
import Dropdown from "core/uikit/Dropdown";
import Image from "core/uikit/Image";
import ItemCard from "core/uikit/ItemCard";
import ModalContainer from "core/uikit/ModalContainer";
import PageHeader from "core/uikit/PageHeader";
import PositionCard from "core/uikit/PositionCard";
import { isChild } from "core/utils/isChild";
import redirectTo from "core/utils/redirectTo";
import { roundToNearestXXMinutes } from "core/utils/roundToNearestXXMinutes";
import moment from "moment";
import type { NextPage } from "next";
import Head from "next/head";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
const TimeSelect = require('react-time-select');
import styles from 'styles/pages/CafePage.module.scss'

const Home: NextPage = () => {
  const dispatch = useDispatch();
  const order = useSelector(order_selector);
  const cafe = useSelector(cafeByID_selector(order.cafeID));
  const user = useSelector(user_selector);
  const total = useSelector(orderCartTotal_selector);

  const [extendedItem, setExtendedItem] = useState<IItemDTO | null>(null);

  const [timeDropdown, setTimedropdown] = useState(false);

  useEffect(() => {
    if (!cafe) {
      dispatch(beginCafeListLoad({
        category: null
      }));
    }
  }, [dispatch, cafe])

  const handleLongItem = useCallback((item) => () => {
    setExtendedItem(item)
  }, [])

  const hadleChangeTake = useCallback((value) => () => {
    if (order && cafe) {
      dispatch(updateOrder({
        table: value,
        cafeID: cafe._id
      }));
    }
  }, [dispatch, order, cafe])

  const dropRef = useRef<HTMLDivElement>(null);

  const handleOpenTimeDrop = useCallback((e) => {
    if (dropRef.current) {
      if (e.target === dropRef.current || isChild(e.target, dropRef.current, 2)) {
        setTimedropdown(true)
      }
    }
  }, [])

  const handleSendOrder = useCallback(() => {
    if (user) {
      dispatch(SendOrder({
        ...order,
        clientID: user._id,
        status: EOrderStatus.created
      }))
    }
  }, [dispatch, order, user])

  return (
    <div>
      {extendedItem && cafe && (
        <ModalContainer onClose={() => setExtendedItem(null)}>

        </ModalContainer>
      )}

      <main className={styles.cafePage}>
        <Appbar />
        <PageHeader
          title={'Моя корзина'}
          backUrl={cafe ? `/cafe/${cafe.code}` : '/'}
        />

        {cafe && order && (
          <>
            <div className={styles.cafePage__imageWrapper}>
              <Image
                className={styles.cafePage__image}
                src={cafe.photo_url || '/images/icons/blank/cafe.svg'}
                alt=""
              />
              <div className={styles.cafePage__badges}>
                <div>
                  {cafe.badges.map((badge, index) => (
                    <Badge
                      key={index}
                      className={styles.cafePage__badge}
                      title={badges.find((item) => item.key == badge)?.label || ''}
                    >
                      <Image
                        src={badges.find((item) => item.key == badge)?.icon || ''}
                        alt=""
                      />
                    </Badge>
                  ))}
                </div>
              </div>
            </div>

            <div className={styles.cafePage__container}>
              <h2>Ваш заказ</h2>

              <div className={styles.cafePage__cartInfoWrapper}>

                <div
                  className={styles.cafePage__cartInfo}
                  onClick={() => {
                    window.open(`https://yandex.ru/maps/?pt=${cafe.location?.lng},${cafe.location?.lat}&z=18&l=map`, "_blank");
                  }}
                >
                  <span className={styles.cafePage__cartInfoLabel}>По адрессу</span>
                  <span className={styles.cafePage__cartInfoValue}>{cafe.address}</span>
                </div>

                <div className={styles.cafePage__cartInfo}>
                  <span className={styles.cafePage__cartInfoLabel}>Способ получения</span>
                  <span className={styles.cafePage__cartInfoValue} onClick={hadleChangeTake(order.table === null ? '' : null)}>
                    {order.table === null ? 'С собой' : 'В заведении'}
                    <Image
                      src="/images/icons/chevron-blue.svg"
                      alt=""
                    />
                  </span>
                </div>

                <div className={styles.cafePage__cartInfo}>
                  <span className={styles.cafePage__cartInfoLabel}>Время получения</span>
                  <span className={styles.cafePage__cartInfoValue} onClick={handleOpenTimeDrop} ref={dropRef}>
                    Как можно скорее
                  </span>
                </div>

                <div className={styles.cafePage__cartInfo}>
                  <span className={styles.cafePage__cartInfoLabel}>Оплата</span>
                  <span className={styles.cafePage__cartInfoValue} onClick={handleOpenTimeDrop} ref={dropRef}>
                    По факту получения
                  </span>
                </div>
              </div>

              <div className={styles.cafePage__cartItems}>
                {order.positions.map((pos) => (
                  <PositionCard
                    key={pos.uuid}
                    position={pos}
                    order={order}
                  />
                ))}
              </div>


              <div className={styles.cafePage__cartTotal}>
                <span>Итог:</span>
                <span>{total?.cost} руб.</span>
              </div>
              <div className={styles.cafePage__cartActions}>
                {user && <Button variant='primary' onClick={handleSendOrder} disabled={total?.cost == 0 || !cafe.acceptOnlineOrders}>
                  {!cafe.acceptOnlineOrders ? 'Кафе не принимает заказы' : 'Оформить заказ'}  
                </Button>}
                {!user && <Button variant='primary' onClick={() => redirectTo('/auth')}>Войдите для оформления</Button>}
              </div>

            </div>
          </>
        )}

      </main>
    </div>
  );
};

export default Home;
