import classNames from "classnames";
import { host } from "core/api/request";
import { ChangeEventHandler, useCallback } from "react";
import Image from "../Image/Image"

import styles from './ImageUploader.module.scss'

interface IProps {
  imageUrl?: string;
  onUpload?: (newValue: string) => void;
  className?: string;
  placeholderUrl?: string;
}

const ImageUploader: React.FC<IProps> = ({
  imageUrl,
  onUpload,
  className,
  placeholderUrl
}) => {
  const handleUpload: ChangeEventHandler<HTMLInputElement> = useCallback(async (e) => {
    if(e.target.files) {
      const formData = new FormData();

      formData.append('file', e.target.files[0])

      const res = await fetch(`${host}/image`, {
        method: 'POST',
        body: formData
      })

      const data = await res.json()

      if(onUpload) onUpload(data.url);
    }
  }, [onUpload])

  return (
    <label className={styles.uploader}>
      <Image
        className={classNames(styles.uploader__image, className)}
        src={imageUrl || ''}
        placeholderUrl={placeholderUrl}
        alt=""
      />
      <input type='file' onChange={handleUpload} className={styles.uploader__input}/>

      <div className={classNames(styles.uploader__overlay, className)}>
        <span>Загрузите изображение</span>
      </div>
    </label>
  )
}

export default ImageUploader;