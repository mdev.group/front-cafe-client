import { ReducersMapObject } from 'redux';

import auth, { IAuthState } from './auth';
import user, { IUsertate } from './user';
import cafe, { ICafeState } from './cafe';
import order, { IOrderState } from './order';

export interface IRootReducer{
    auth: IAuthState,
    user: IUsertate,
    cafe: ICafeState,
    order: IOrderState,
}

const reducers: ReducersMapObject<IRootReducer, any> = {
    auth,
    user,
    cafe,
    order
};

export default reducers;
