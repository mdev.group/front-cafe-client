import { ICafeDTO } from 'core/redux/types/dto/cafe';
import { IOrder } from 'core/redux/types/dto/order';
import { TFunc } from 'core/types/types';
import { mapStatusToString } from 'core/utils/mapStatusToString';
import Badge from '../Badge';
import styles from './OrderCard.module.scss';

interface IProps {
  order: IOrder;
  total?: {
    cost: number;
  }
  cafe?: ICafeDTO;
  onClick?: TFunc;
}

const OrderCard: React.FC<IProps> = ({
  order,
  total,
  cafe,
  onClick
}) => {
  return (
    <div className={styles.card} onClick={onClick}>
      <span className={styles.card__name}>Заказ #{order.number}</span>
      <div className={styles.card__badges}>
        <Badge>
          {mapStatusToString(order.status)}
        </Badge>
        <Badge>
          {order.table === null ? 'С собой' : 'В заведении'}
        </Badge>
      </div>

      <div>
        <div className={styles.card__infoItem}>
          <span className={styles.card__title}>Заведение</span>
          <span className={styles.card__value}>{cafe?.name}</span>
        </div>

        <div className={styles.card__infoItem}>
          <span className={styles.card__title}>Сумма заказа</span>
          <span className={styles.card__value}>{total?.cost} руб.</span>
        </div>
      </div>
    </div>
  )
}

export default OrderCard;