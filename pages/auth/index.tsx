import type { NextPage } from "next";
import Head from "next/head";
import { useCallback, useEffect } from "react";
import authTokenService from "core/services/authTokenService";
import redirectTo from "core/utils/redirectTo";
import TgAuth from "core/uikit/TgAuth/TgAuth";

import styles from "styles/pages/AuthPage.module.scss";
import { useDispatch } from "react-redux";
import { userAuth } from "core/redux/actions/auth";

const Home: NextPage = () => {
  const dispatch = useDispatch();

  const onTgAuth = useCallback((userdata) => {
    dispatch(userAuth({
      ...userdata,
      userType: 'client'
    }))
  }, [dispatch])

  useEffect(()=>{
    if(authTokenService.hasTokenInLocalstorage()) {
      redirectTo('/');
    }
  }, [])
  
  return (
    <div className={styles.page}>
      <Head>
        <title>MD.Resto | Авторизация</title>
      </Head>
      <main className={styles.main}>
        <div className={styles.authCard}>
          <span className={styles.authCard__brand}>MD.Cafe</span>

          <div className={styles.authCard__content}>
            <div className={styles.authCard__contentInner}>
              <h1 className={styles.authCard__title}>Авторизация</h1>
              <h2 className={styles.authCard__subtitle}>Вход в систему</h2>
            </div>
            <hr />
            <div className={styles.authCard__contentInner}>
              <TgAuth onAuth={onTgAuth}/>
            </div>
          </div>

          <span className={styles.authCard__copy}>Powered by M.Dev</span>
        </div>
      </main>
    </div>
  );
};

export default Home;
