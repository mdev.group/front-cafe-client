export function numToLength(number: number, n: number) {  
  return String(number).padStart(n, '0');
}