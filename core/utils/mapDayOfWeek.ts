export const mapDayOfWeek = (day: number) => {
  const labels = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
  return labels[day - 1];
}

