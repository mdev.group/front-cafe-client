import { useForm } from "core/hooks/useForm";
import { updateOrder } from "core/redux/actions/order";
import { order_selector } from "core/redux/selectors";
import { ICafeDTO } from "core/redux/types/dto/cafe";
import { ICompositionAssign, IItemDTO } from "core/redux/types/dto/item";
import { IOrderItemIngredients, IOrderPosition } from "core/redux/types/dto/order";
import { uniqueId } from "lodash";
import { useCallback, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "../Button";
import CountButton from "../CountButton";
import Image from "../Image";
import ModalContainer from "../ModalContainer";

import styles from './ExtendItemCard.module.scss';

type ExtendItemCardProps = {
  className?: string;

  item: IItemDTO;
  cafe: ICafeDTO;

  onSave?: (UID: string, item?: any) => void;
  onClose?: () => void;
};

export const ExtendItemCard: React.FC<ExtendItemCardProps> = ({
  onClose,
  item,
  cafe
}) => {
  const dispatch = useDispatch();
  const order = useSelector(order_selector);

  const handleRemove = () => {

  };

  const handleSave = (formData: any) => {

  };
  
  const [currIngredients, setIngredients] = useState(
    item.composition.map((assign) => {
      const compData = item.compositionItems.find((comp) => comp._id === assign.id);
      
      if(compData) {
        return {
          id: assign.id,
          defaultCount: assign.isEditable ? 0 : 1,
          count: assign.isEditable ? 0 : 1,
          name: compData.name,
          price: assign.price || 0,
          isEditable: assign.isEditable,
          massStep: item.mass
        }
      }
  
      return undefined!;
    }).filter((data) => data !== undefined).map((ing) => ({
      ...ing,
      count: ing.count !== undefined ? ing.count : ing.defaultCount,
    }))
  );

  const handleAdd = useCallback(() => {
    dispatch(updateOrder({
      positions: [...(order.cafeID !== cafe._id ? [] : order.positions), {
        itemID: item._id,
        orderTime: Number(new Date()),
        ingredients: currIngredients,
        uuid: uniqueId(),
        fixed: false
      }],
      cafeID: cafe._id
    }));

    if (onClose) onClose();
  }, [item, currIngredients, dispatch, cafe, order, onClose]);

  const totalPFC = useMemo(() => {
    return currIngredients.reduce((prev, ing) => {
      const compData = item.compositionItems.find((comp) => comp._id === ing.id);
      if(compData && compData.pfc) {
        const {proteins, fats, carbohydrates} = compData.pfc;
        prev.proteins += +proteins * (ing.count * ing.massStep / 100);
        prev.fats += +fats * (ing.count * ing.massStep / 100);
        prev.carbohydrates += +carbohydrates * (ing.count * ing.massStep / 100);
      }
      return prev;
    }, {
      proteins: 0, fats: 0, carbohydrates: 0
    })
  }, [item, currIngredients]);

  const ingPrice = useMemo(() => {
    return currIngredients.reduce((prev, curr) => {
      return prev + (Math.max(curr.count - curr.defaultCount, 0) * curr.price);
    }, 0)
  }, [currIngredients])

  return (
    <div>
      <span className={styles.card__title}>{item.name}</span>
      <span className={styles.card__mass}>{item.mass} гр.</span>
      <Image
        src={item.photo_url}
        alt=""
        className={styles.card__image}
      />

      <div className={styles.card__ingredients}>
        {totalPFC && (totalPFC.proteins > 0 || totalPFC.fats > 0 || totalPFC.carbohydrates > 0) && (
          <div className={styles.card__pfcWrapper}>
            <div className={styles.card__pfc}>
              <span className={styles.card__pfcTitle}>Белки</span>
              <span>{totalPFC.proteins.toFixed(1)}</span>
            </div>
            <div className={styles.card__pfc}>
              <span className={styles.card__pfcTitle}>Жиры</span>
              <span>{totalPFC.fats.toFixed(1)}</span>
            </div>
            <div className={styles.card__pfc}>
              <span className={styles.card__pfcTitle}>Углеводы</span>
              <span>{totalPFC.carbohydrates.toFixed(1)}</span>
            </div>
          </div>
        )}
        {currIngredients.map((ing) => (
            <div key={ing.id} className={styles.card__comp}>
              <CountButton
                  value={ing.count}
                  title={ing.name}
                  subtitle={ing.price > 0 && `${ing.price} руб.` || 'Бесплатно'}
                  key={ing.id}
                  canAdd
                  onAdd={() => {
                    setIngredients([
                      ...currIngredients.map((updIng) =>
                        updIng.id == ing.id
                          ? {
                              ...updIng,
                              count: updIng.count + 1,
                            }
                          : updIng
                      ),
                    ]);
                  }}
                  onSub={() => {
                    setIngredients([
                      ...currIngredients.map((updIng) =>
                        updIng.id == ing.id
                          ? {
                              ...updIng,
                              count: updIng.count - 1,
                            }
                          : updIng
                      ),
                    ]);
                  }}
              />
            </div>
        ))}
      </div>

      <div className={styles.card__actions}>
        <span className={styles.card__price}>{item?.price + ingPrice} ₽</span>
        <Button variant='primary' onClick={handleAdd}>
          Добавить в корзину
        </Button>
      </div>
    </div>
  );
};
