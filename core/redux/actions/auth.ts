import { createAction } from "redux-actions";
import { IRefreshData } from "../reducers/auth";

export type setToken_payload = {
  token: string | undefined;
};
export const setToken = createAction<setToken_payload>(
  'SET_TOKEN'
);

export type setRefreshData_payload = {
  refreshData: IRefreshData | undefined;
};
export const setRefreshData = createAction<setRefreshData_payload>(
  'SET_REFRESH_DATA'
);

export const setRefreshed = createAction(
  'SET_REFRESHED'
);

export const appClearAuth = createAction(
  'APPLICATION_CLEAR_AUTH'
);

export const refreshToken = createAction(
  'REFRESH_TOKEN'
);

export const userAuthByStoredData = createAction(
  'AUTH_BY_STORED_DATA'
);

export const userAuth = createAction(
  'AUTH_USER'
);

export const userAuthEnd = createAction(
  'AUTH_USER_END'
);

export const userAuthSuccess = createAction(
  'AUTH_USER_SUCCESS'
);

export const userRegister = createAction(
  'REGISTER_USER'
);

export const userRegisterSuccess = createAction(
  'REGISTER_USER_SUCCESS'
);

export type userRegisterFail_payload = {
  error: string;
};
export const userRegisterFail = createAction<userRegisterFail_payload>(
  'REGISTER_USER_FAIL'
);


export type userAuthFail_payload = {
  error: string;
};
export const userAuthFail = createAction<userAuthFail_payload>(
  'AUTH_USER_FAIL'
);

export const authInitializeSuccess = createAction(
  'AUTH_INITIALIZE_SUCCESS'
);