import classNames from "classnames";
import { IItemDTO } from "core/redux/types/dto/item"
import Image from "core/uikit/Image"
import { LongPressDetectEvents, useLongPress } from 'use-long-press';
import { useDebouncedCallback  } from 'use-debounce';

import styles from './ItemCard.module.scss';
import { useCallback, useState } from "react";

interface IProps {
  data: IItemDTO;
  onClick?: () => void;
  count?: number;
}

const ItemCard: React.FC<IProps> = ({
  data,
  onClick,
  count = 0
}) => {
 
  return (
    <div
      className={classNames(styles.card, {
        [styles['card--active']]: count > 0
      })}
      onClick={onClick}
    >
      <Image
        className={styles.card__image}
        src={data.photo_url}
        alt=""
      />
      <div className={styles.card__price}>{`${data.price} ₽`}</div>
      <div className={styles.card__info}>
        <span className={styles.card__title}>{data.name}</span>
        <span className={styles.card__mass}>{`${data.mass}\u00A0гр.`}</span>
        {count > 0 && <span className={styles.card__count}>{`x${count}`}</span>}
      </div>
    </div>
  )
}

export default ItemCard;