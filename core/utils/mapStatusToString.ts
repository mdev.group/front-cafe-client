import { EOrderStatus } from "core/redux/types/dto/order";

const assign = {
  [EOrderStatus.form]: 'Формируется',
  [EOrderStatus.created]: 'Создан',
  [EOrderStatus.approved]: 'Подтвержден',
  [EOrderStatus.serve]: 'Обслуживание',
  [EOrderStatus.checktime]: 'На расчете',
  [EOrderStatus.loyalty]: 'На расчете',
  [EOrderStatus.scoresChoose]: 'На расчете',
  [EOrderStatus.paySelection]: 'На расчете',
  [EOrderStatus.paytime]: 'Оплата',
  [EOrderStatus.done]: 'Закрыт'
} as {[key: string]: string}

export const mapStatusToString = (status: EOrderStatus) => {
  return assign[status];
}