import { useForm } from 'core/hooks/useForm';
import { beginCompositionCreate, beginCompositionSearch } from 'core/redux/actions/composition';
import { beginItemUpdate } from 'core/redux/actions/item';
import { getItemFromSelectedCafe_selector, searchedCompositions_selector } from 'core/redux/selectors';
import { IComposition } from 'core/redux/types/dto/item';
import Button from 'core/uikit/Button';
import Input from 'core/uikit/Input';
import ModalContainer, { ModalForm } from 'core/uikit/ModalContainer';
import SearchInput from 'core/uikit/SearchInput/SearchInput';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './NewCompositionModal.module.scss';

interface IProps {
  onClose: () => void;
  itemID: string;
}

const NewCompositionModal: React.FC<IProps> = ({
  onClose,
  itemID
}) => {
  const dispatch = useDispatch();

  const searchedItems = useSelector(searchedCompositions_selector)
  const itemData = useSelector(getItemFromSelectedCafe_selector(itemID))

  const handleCreateCompositon = useCallback((formData) => {
    dispatch(beginCompositionCreate({
      name: formData.name,
      pfc: {
        proteins: formData['pfc'].proteins.toString(),
        fats: formData['pfc'].fats.toString(),
        carbohydrates: formData['pfc'].carbohydrates.toString()
      },
      itemID
    }))

    if(onClose) onClose();
  }, [dispatch, onClose])

  const {
    onChangeField,
    handleSubmit,
    formData
  } = useForm(handleCreateCompositon, {
    pfc: {
      proteins: '0',
      fats: '0',
      carbohydrates: '0'
    }
  });

  const handleUpdateCompositionSearch = useCallback((text) => {
    dispatch(beginCompositionSearch({
      searchText: text
    }))
  }, [dispatch])

  const [selectedItem, setSelectedItem] = useState<IComposition | null>(null)

  const handleSelect = useCallback(() => {
    if(itemData && selectedItem) {
      dispatch(beginItemUpdate({
        id: itemData._id,
        newData: {
          composition: [...itemData.composition, {
            id: selectedItem._id,
            mass: 100,
            isEditable: false
          }]
        }
      }))

      if(onClose) onClose();
    }
  }, [onClose, dispatch, itemData, selectedItem])

  return (
    <ModalContainer
      onClose={onClose}
      title="Создание ингредиента"
    >
      <ModalForm>
        <SearchInput
          label="Название"
          onChange={onChangeField('name')}
          value={selectedItem?.name || formData['name']}
          items={searchedItems?.map((item) => ({
            label: item.name,
            onClick: () => setSelectedItem(item)
          })) || []}
          onUpdateSearch={handleUpdateCompositionSearch}
        />

        <div className={styles.form__pfc}>
          <Input
            label="Белки"
            name="proteins"
            value={selectedItem?.pfc?.proteins || formData['pfc'].proteins}
            disabled={!!selectedItem}
            onChange={onChangeField('pfc.proteins')}
          />
          <Input
            label="Жиры"
            name="fats"
            value={selectedItem?.pfc?.fats ||formData['pfc'].fats}
            disabled={!!selectedItem}
            onChange={onChangeField('pfc.fats')}
          />
          <Input
            label="Углеводы"
            name="carbohydrates"
            value={selectedItem?.pfc?.carbohydrates ||formData['pfc'].carbohydrates}
            disabled={!!selectedItem}
            onChange={onChangeField('pfc.carbohydrates')}
          />
        </div>

        <Button
          size="md"
          theme="primary"
          onClick={!!selectedItem ? handleSelect : handleSubmit}
        >
          {!!selectedItem ? 'Добавить' : 'Создать и добавить'}
        </Button>
      </ModalForm>
    </ModalContainer>
  )
}

export default NewCompositionModal;