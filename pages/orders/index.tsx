import { badges } from "core/constants/categories";
import { beginCafeListLoad } from "core/redux/actions/cafe";
import { SendOrder, updateOrder } from "core/redux/actions/order";
import { cafeByID_selector, cafes_selector, orderCartTotal_selector, ordersListTotals_selector, ordersList_selector, order_selector, user_selector } from "core/redux/selectors";
import { IItemDTO } from "core/redux/types/dto/item";
import { EOrderStatus } from "core/redux/types/dto/order";
import Appbar from "core/uikit/Appbar/Appbar";
import Badge from "core/uikit/Badge";
import Button from "core/uikit/Button";
import Dropdown from "core/uikit/Dropdown";
import Image from "core/uikit/Image";
import ItemCard from "core/uikit/ItemCard";
import ModalContainer from "core/uikit/ModalContainer";
import OrderCard from "core/uikit/OrderCard";
import PageHeader from "core/uikit/PageHeader";
import PositionCard from "core/uikit/PositionCard";
import { isChild } from "core/utils/isChild";
import redirectTo from "core/utils/redirectTo";
import { roundToNearestXXMinutes } from "core/utils/roundToNearestXXMinutes";
import moment from "moment";
import type { NextPage } from "next";
import Head from "next/head";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
const TimeSelect = require('react-time-select');
import styles from 'styles/pages/CafePage.module.scss'

const Home: NextPage = () => {
  const dispatch = useDispatch();
  const user = useSelector(user_selector);
  const orders = useSelector(ordersList_selector);
  const totals = useSelector(ordersListTotals_selector);
  const cafes = useSelector(cafes_selector);

  useEffect(() => {
    dispatch(beginCafeListLoad({
      category: null,
      ids: orders.reduce((prev, ord) => {
        return [...prev, ord.cafeID]
      }, [] as string[])
    }));
  }, [orders, dispatch])

  return (
    <div>
        <main className={styles.cafePage}>
          <Appbar />
          <PageHeader
            title={'Мои заказы'}
            backUrl="/profile"
          />

      
          <div className={styles.cafePage__ordersList}>
            {user && orders && (
                orders.sort((a,b) => b.beginTime - a.beginTime).map((order) => {
                  const total = totals?.find((total) => total.orderID === order._id);

                  return (
                    <OrderCard
                      key={order._id}
                      order={order}
                      total={total}
                      cafe={cafes.find((cafe) => cafe._id === total?.cafeID)}
                      onClick={() => redirectTo(`/orders/${order._id}`)}
                    />
                  )
                })
            )}
          </div>

        </main>
    </div>
  );
};

export default Home;
