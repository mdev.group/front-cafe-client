import { ICafeDTO } from "core/redux/types/dto/cafe";

export const calculateEmployeeTime = (employeeID: string, selectedCafe: ICafeDTO) => {
  if(selectedCafe) {
    return selectedCafe.workshifts.reduce((prev, curr) => {
      if(curr.employeeIDs.includes(employeeID)) {
        return prev + (curr.endTime - curr.beginTime);
      }
      return prev;
    }, 0)
  }

  return 0;
}