import type { ICafeDTO } from "./cafe";
import type { IWorkShiftDTO } from "./workshift";

export interface IEmployeeDTO {
  _id: string;
  cafeID: string;
  tgID: number;
  first_name: string;
  last_name: string;
  secret: string;
  hireDate: number;
  photo_url?: string;
  fireDate: number | null;
  position: string;
}