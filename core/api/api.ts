
import { ICafeDTO } from 'core/redux/types/dto/cafe';
import { IClient } from 'core/redux/types/dto/client';
import { IOrder } from 'core/redux/types/dto/order';
import { IUserDTO } from 'core/redux/types/dto/user';
import getApi from './getApi';
import { refreshToken } from './utils';

export enum ERequestTypes {
    GET = 'get',
    POST = 'post',
    PATCH = 'patch',
    DELETE = 'delete'
}

export interface IApi {
    refreshToken: () => any;

    authUser: (data: any) => any;

    registerUser: (data: {
        email: string
    }) => any;

    getUserData: () => IUserDTO;
    getCafeList: () => ICafeDTO[];
    sendOrder: (data: Partial<IOrder>) => IOrder;
    getOrders: () => IOrder[];
    updateUser: (data: Partial<IClient>) => IClient;
}

const api: IApi = getApi({
    refreshToken: {
        requestHandler: refreshToken
    },
    authUser: {
        path: () => '/auth/login/client',
        type: ERequestTypes.POST
    },

    registerUser: {
        path: () => '/auth/register',
        type: ERequestTypes.POST
    },

    getUserData: {
        path: () => '/user/self',
        type: ERequestTypes.GET
    },

    getCafeList: {
        path: () => '/cafe/get',
        type: ERequestTypes.POST
    },

    sendOrder: {
        path: () => '/order',
        type: ERequestTypes.POST
    },

    getOrders: {
        path: () => '/order/client',
        type: ERequestTypes.GET
    },

    updateUser: {
        path: () => '/client',
        type: ERequestTypes.PATCH
    },
});

export default api;