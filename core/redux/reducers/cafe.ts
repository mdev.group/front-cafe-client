import { createSelector } from '@reduxjs/toolkit';
import { handleActions, Action } from 'redux-actions';
import { successCafeListLoad } from '../actions/cafe';

import { setOwnedCafes, setUserData } from '../actions/user';

import type { ICafeDTO } from '../types/dto/cafe';
import { IEmployeeDTO } from '../types/dto/employee';
import { IComposition } from '../types/dto/item';
import type { IUserDTO } from '../types/dto/user';

export interface ICafeState {
  list: ICafeDTO[];
}

const initialState: ICafeState = {
  list: [],
};

// TODO: remove all any
const reducer = handleActions<ICafeState, any>({

  [successCafeListLoad.toString()]: (state, {payload}: Action<ICafeDTO[]>) => ({
    ...state,
    list: payload.reduce((prev, curr) => {
      const itemIndex = prev.findIndex((it) => it._id === curr._id);
      if(itemIndex !== -1) {
        prev.splice(itemIndex, 1, curr)
        return prev; 
      }
      return [...prev, curr];
    }, state.list)
  }),

}, initialState);

export default reducer;
