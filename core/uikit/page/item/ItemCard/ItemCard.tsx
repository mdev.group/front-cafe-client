import { IItemDTO } from "core/redux/types/dto/item"
import Image from "core/uikit/Image"

import styles from './ItemCard.module.scss';

interface IProps {
  data: IItemDTO;
}

const ItemCard: React.FC<IProps> = ({
  data
}) => {
  return (
    <div className={styles.card}>
      <Image
        className={styles.card__image}
        src={data.photo_url || "/images/icons/blank/food.svg"}
        alt=""
      />
      <div className={styles.card__price}>{`${data.price} ₽`}</div>
      <div className={styles.card__info}>
        <span className={styles.card__title}>{data.name}</span>
        <span className={styles.card__mass}>{`${data.mass}\u00A0гр.`}</span>
      </div>
    </div>
  )
}

export default ItemCard;