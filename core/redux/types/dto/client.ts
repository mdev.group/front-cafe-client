export interface IClient {
  _id: string;
  tgID: number;
  code: string;
  first_name: string;
  last_name: string;
  photo_url: string;
  info: string;
  reg_datetime: number;
  scores: number;
  birth_date?: number;
}