export const isChild = (obj: HTMLElement, parentObj: HTMLElement, maxDeep = 999): boolean => {
  let deep = 0;
  let currObj = obj;
  while (deep < maxDeep && currObj !== undefined && currObj !== null && currObj.tagName.toUpperCase() !== 'BODY' && currObj.tagName.toUpperCase() !== 'HTML') {
      if (currObj === parentObj) {
          return true;
      }
      deep += 1;
      currObj = currObj.parentNode as HTMLElement;
  }
  return false;
};
