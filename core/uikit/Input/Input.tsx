import styles from "./Input.module.scss";
import cn from 'classnames';
import { ChangeEvent, FC, useEffect, useMemo, useRef, useState } from "react";
import { AddressSuggestions } from 'react-dadata';
import 'react-dadata/dist/react-dadata.css';
import { mapDayOfWeek } from "core/utils/mapDayOfWeek";
import classNames from "classnames";
import moment from "moment";

type TTypes = 'password' | 'text' | 'address' | 'dayOfWeek' | 'time' | 'date';

interface IProps {
  label?: string;
  type?: TTypes,
  name?: string,
  onChange?: (newValue: string, ev?: ChangeEvent)=>void
  value?: any;
  className?: string;
  required?: boolean;
  disabled?: boolean;
  placeholder?: string;
}

const Input:FC<IProps> = ({
  type = 'text',
  label,
  name,
  onChange,
  value: controlledValue,
  className,
  disabled,
  placeholder
}) => {
  const [focus, setFocus] = useState(false);
  const [value, setValue] = useState<any>('');

  const fieldValue = useMemo(()=>{
    return controlledValue || value;
  }, [value, controlledValue])

  const handleChange = (e: ChangeEvent)=>{
    let newValue = (e.target as HTMLInputElement).value as any;

    if(type === 'date') {
      newValue = moment(newValue).unix() * 1000
    }

    setValue(newValue);
    if(onChange) onChange(newValue, e)
  }

  const seterNewValue = (value: any) => () =>{
    setValue(value);
    if(onChange) onChange(value)
  }

  const formattedValue = useMemo(() => {
    if (type === 'date') {
      return moment(fieldValue).format("YYYY-MM-DD")
    }
    return fieldValue
  }, [fieldValue, type])

  if(type === 'dayOfWeek') {
    return (
      <div className={cn(styles.dayInput, className, {
        [styles.input_focused]: focus,
        [styles.input_filled]: formattedValue,
        [styles.input_nolabel]: !label,
        [styles.input_disabled]: disabled,
      })}>
        {[...Array(7)].map((_, index) => (
          <button
            key={index}
            className={classNames(
              styles.dayInput__item,
              {
                [styles['dayInput__item--active']]: formattedValue === index + 1
              }
            )}
            onClick={!disabled ? seterNewValue(index + 1) : undefined}
          >
            {mapDayOfWeek(index + 1)}
          </button>
        ))}
      </div>
    );
  }

  return (
    <div className={cn(styles.input, className, {
      [styles.input_focused]: focus,
      [styles.input_filled]: formattedValue || type === 'time',
      [styles.input_nolabel]: !label,
      [styles.input_disabled]: disabled,
    })}>
      <span className={styles.label}>{label}</span>
      <input
        className={styles.field}
        type={type}
        value={formattedValue}
        name={name}
        autoComplete="new-password"
        onFocus={()=>{setFocus(true)}}
        onBlur={()=>{setFocus(false)}}
        onChange={handleChange}
        required
        disabled={disabled}
        placeholder={placeholder}
      />
    </div>
  );
};

export default Input;
