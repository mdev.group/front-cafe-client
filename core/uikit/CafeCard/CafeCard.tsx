import classNames from "classnames";
import { badges } from "core/constants/categories";
import type { ICafeDTO } from "core/redux/types/dto/cafe";
import { TFunc } from "core/types/types";
import Badge from "../Badge";
import Image from "../Image";
import { useCurrentPosition } from 'react-use-geolocation';

import styles from './CafeCard.module.scss'
import { useCallback, useMemo, useState } from "react";
import { getDistanceFromLatLonInKm } from "core/utils/getDistanceFromLatLonInKm";
import Button from "../Button";

interface IProps {
  data: ICafeDTO;
  onClick?: TFunc;
}

const CafeCard: React.FC<IProps> = ({
  data,
  onClick
}) => {
  const costRating = 2;

  const [position, error] = useCurrentPosition();

  const distance = useMemo(() => {
    if(!position || !data.location) {
      return null
    }
    const {latitude, longitude} = position.coords
    const distanceInM = getDistanceFromLatLonInKm(latitude, longitude, Number(data.location.lat), Number(data.location.lng)) * 1000
    return distanceInM.toFixed(0);
  }, [position, data])

  const handleLike = useCallback((e) => {
    e.preventDefault();
    e.stopPropagation();

    let favors = JSON.parse(localStorage.getItem('favorites') || '[]');

    if(!favors.includes(data._id)) {
      favors.push(data._id);
      setFavorite(true)
    } else {
      favors = favors.filter((it: string) => it !== data._id);
      setFavorite(false)
    }
    localStorage.setItem('favorites', JSON.stringify(favors))

  }, [data])

  const [favorite, setFavorite] = useState(() => {
    return JSON.parse(localStorage.getItem('favorites') || '[]').includes(data._id)
  })

  return (
    <div className={styles.cafeCard} onClick={onClick}>
      <div className={styles.cafeCard__imageWrapper}>
        <Image
          className={styles.cafeCard__image}
          src={data.photo_url || '/images/icons/blank/cafe.svg'}
          alt=""
        />
        <div className={styles.cafeCard__badges}>
          <div>
            {data.badges.map((badge, index) => (
              <Badge
                key={index}
                className={styles.cafeCard__badge}
                title={badges.find((item) => item.key == badge)?.label || ''}
              >
                <Image
                  src={badges.find((item) => item.key == badge)?.icon || ''}
                  alt=""
                />
              </Badge>
            ))}
          </div>
        </div>

        <div>
          <Button
          variant='secondary'
          className={classNames(styles.cafeCard__likeBtn, {
            [styles['cafeCard__likeBtn--active']]: favorite
          })}
          onClick={handleLike}
        >
            <Image
              src={favorite ? "/images/icons/like-red.svg" : "/images/icons/like.svg"}
              alt=""
            />
          </Button>
        </div>
      </div>

      <div className={styles.cafeCard__info}>
        <span className={styles.cafeCard__name}>{data.name}</span>
        <div className={styles.cafeCard__cats}>
          {data.categories.map((item, index) => (
            <span key={index}>{item}</span>
          ))}
        </div>
        <div className={styles.cafeCard__badges}>
          <div>
            {distance && <Badge>~ {distance} м</Badge>}
            {/* <Badge>
              <span className={styles.cafeCard__costRating}>{[...Array(costRating)].map(() => '₽')}</span>
              <span className={styles.cafeCard__costRating}>{[...Array(3 - costRating)].map(() => '₽')}</span>
            </Badge> */}
          </div>

          <div>
            {data.acceptOnlineOrders && <Badge>Закажи онлайн</Badge>}
          </div>
        </div>
      </div>
    </div>
  )
}

export default CafeCard;