import redirectTo from 'core/utils/redirectTo';
import { takeEvery, put, call } from 'redux-saga/effects'
import api from '../../api';
import authTokenService from '../../services/authTokenService';

import {
  refreshToken as refreshTokenAction,
  userAuth as userAuthAction,
  userRegister as userRegisterAction,
  userAuthEnd,
  userAuthFail,
  userAuthSuccess,
  userRegisterSuccess,
  userRegisterFail
} from '../actions/auth';
import { requestError } from '../actions/common';
import { IRootReducer } from '../reducers';
import { waitFor } from './common';

const setAuthData = (access_token: string, expires_in: number, refresh_token: string): void => {
  authTokenService.setToken(access_token);
  authTokenService.setRefreshData({
      accessTokenLifeTime: expires_in,
      refreshToken: refresh_token
  });
};

function* refreshToken(): Generator {
  try {
      const selectRefreshDataState = (state:IRootReducer) => state.auth.refreshData;
      yield call(waitFor, selectRefreshDataState);
      console.log('[redux] start refresh')
      yield call(api.refreshToken);
      return;
  } catch (error) {
      yield put(requestError(error));
  }
}

function* authUser({payload}: any): Generator {
  try {
      const response: any = yield call(api.authUser, payload);

      yield put(userAuthEnd());
      yield put(userAuthSuccess());

      if (response.access_token) {
          setAuthData(response.access_token, response.expires_in, response.refresh_token);
          redirectTo('/');
      }
  } catch (error) {
      yield put(userAuthEnd())
      yield put(userAuthFail({
        error: "Неверный логин или пароль"
      }));
      yield put(requestError(error));
  }
}

function* registerUser({payload}: any): Generator {
  try {
      const response: any = yield call(api.registerUser, {
        email: payload.email.trim()
      });

      if(response.message === "REG_SUCCESS") {
        yield put(userRegisterSuccess());
        redirectTo('/auth');
        return;
      }

      if(response.message === "ALREADY_EXISTS") {
        yield put(userRegisterFail({
          error: "Пользователь уже существует"
        }));
        return;
      }

      if(response.message === "INVALID_EMAIL") {
        console.error(response)
        yield put(userRegisterFail({
          error: "Некорректно введен Email"
        }));
        return;
      }
  } catch (error) {
      yield put(userRegisterFail({
        error: "Неизвестная ошибка регистрации"
      }));
      yield put(requestError(error));
  }
}

export function* initAuthWatchers(){
  yield takeEvery(userAuthAction.toString(), authUser);
  yield takeEvery(userRegisterAction.toString(), registerUser);
  yield takeEvery(refreshTokenAction.toString(), refreshToken);
}