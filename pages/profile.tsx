import { useForm } from "core/hooks/useForm";
import { updateUser } from "core/redux/actions/user";
import { user_selector } from "core/redux/selectors";
import Appbar from "core/uikit/Appbar/Appbar";
import Button from "core/uikit/Button";
import Image from "core/uikit/Image";
import ImageUploader from "core/uikit/ImageUploader";
import Input from "core/uikit/Input";
import ModalContainer from "core/uikit/ModalContainer";
import PageHeader from "core/uikit/PageHeader";
import { declOfNum } from "core/utils/devlOfNum";
import redirectTo from "core/utils/redirectTo";
import moment from "moment";
import type { NextPage } from "next";
import Head from "next/head";
import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { QRCodeSVG } from 'qrcode.react';

import styles from 'styles/pages/ProfilePage.module.scss'
import authTokenService from "core/services/authTokenService";

const Home: NextPage = () => {
  const dispatch = useDispatch();

  const user = useSelector(user_selector);

  const [isEdit, setEdit] = useState(false);

  const handleSave = useCallback((formData) => {
    dispatch(updateUser({
      ...formData
    }))
    setEdit(false);
  }, [dispatch])

  const {
    formData,
    handleSubmit,
    onChangeField,
    isChanged,
    reinit
  } = useForm(handleSave, {
    ...user
  });

  useEffect(() => {
    reinit()
  }, [user])

  const [showQRModal, setShowQRModal] = useState(false);

  const handleUnauth = ()=>{
    authTokenService.clearRefreshData();
    authTokenService.clearToken();
    redirectTo('/auth');
  }

  return (
    <>
      {showQRModal && <ModalContainer hideCloseIcon={true} onClose={() => {setShowQRModal(false)}}>
        <QRCodeSVG value={`{"code":"${user?.code}"}`} size={300} radius={50} />,
      </ModalContainer>}
      <div>
        <main>
          <Appbar />
          <PageHeader title="Личный кабинет" />

          <div className={styles.page}>
            {user && (
              <>
                {isEdit ? (
                  <ImageUploader
                    imageUrl={formData.photo_url}
                    onUpload={onChangeField('photo_url')}
                    className={styles.page__photo}
                  />
                ) : (
                  <Image
                    src={formData.photo_url}
                    alt=''
                    className={styles.page__photo}
                  />
                )}
                {!isEdit && (
                  <h2>{[user.first_name, user.last_name].join(' ')}</h2>
                )}

                <div className={styles.page__info}>
                  {!isEdit && (
                    <div className={styles.page__infoItem}>
                      <span className={styles.page__infoItemTitle}>
                        Telegram ID
                      </span>
                      <span className={styles.page__infoItemValue}>
                        {user.tgID}
                      </span>
                    </div>
                  )}

                  {isEdit && (
                    <>
                      <div className={styles.page__infoItem}>
                        <span className={styles.page__infoItemTitle}>
                          Имя
                        </span>
                        <span className={styles.page__infoItemValue}>
                          <Input
                            value={formData.first_name || ''}
                            onChange={onChangeField('first_name')}
                          />
                        </span>
                      </div>

                      <div className={styles.page__infoItem}>
                        <span className={styles.page__infoItemTitle}>
                          Фамилия
                        </span>
                        <span className={styles.page__infoItemValue}>
                          <Input
                            value={formData.last_name || ''}
                            onChange={onChangeField('last_name')}
                          />
                        </span>
                      </div>
                    </>
                  )}

                  <div className={styles.page__infoItem}>
                    <span className={styles.page__infoItemTitle}>
                      Дата рождения
                    </span>
                    <span className={styles.page__infoItemValue}>
                      {isEdit ? (
                        <Input
                          value={formData.birth_date || ''}
                          onChange={onChangeField('birth_date')}
                          type="date"
                        />
                      ) : (user.birth_date ? moment(user.birth_date).format('DD.MM.yyyy') : '-')}
                    </span>
                  </div>

                  <div className={styles.page__infoItem}>
                    <span className={styles.page__infoItemTitle}>
                      О вас
                    </span>
                    <span className={styles.page__infoItemValue}>
                      {isEdit ? (
                        <Input
                          value={formData.info || ''}
                          onChange={onChangeField('info')}
                          placeholder="Ваши аллергии или другая полезная информация"
                        />
                      ) : (user.info || '-')}
                    </span>
                  </div>

                  {!isEdit && <div className={styles.page__infoItem}>
                    <span className={styles.page__infoItemTitle}>
                      Код лояльности
                    </span>
                    <span className={styles.page__infoItemValue}>
                     {user.code}
                    </span>
                  </div>}

                  {!isEdit && (
                    <div className={styles.page__infoItem}>
                      <span className={styles.page__infoItemTitle}>
                        Баллы лояльности
                      </span>
                      <span className={styles.page__infoItemValue}>
                        {user.scores} {declOfNum(user.scores, ['балл', 'балла', 'баллов'])}
                      </span>
                    </div>
                  )}
                </div>

                <div className={styles.page__actions}>
                  {isEdit ? (
                    <>
                      <Button variant='primary' onClick={handleSubmit} disabled={!isChanged}>Сохранить профиль</Button>
                      <Button variant='secondary' onClick={() => setEdit(false)}>Отменить</Button>
                    </>
                  ) : (
                    <>
                      <Button variant='primary' onClick={() => setShowQRModal(true)}>QR лояльности</Button>
                      <Button variant='primary' onClick={() => redirectTo('/orders')}>История заказов</Button>
                      <Button variant='secondary' onClick={() => setEdit(true)}>Обновить профиль</Button>
                      <Button variant='secondary' onClick={handleUnauth}>Выйти из аккаунта</Button>
                    </>
                  )}
                </div>
              </>
            )}
          </div>
        </main>
      </div>
    </>
  );
};

export default Home;
