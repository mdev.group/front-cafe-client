import classNames from 'classnames';
import styles from './Badge.module.scss';

interface IProps {
    className?: string;
    title?: string;
}

const Badge: React.FC<IProps> = ({
    children,
    className,
    title
}) => {
    return (
        <div className={classNames(className, styles.badge)}>
            {children}
            {title && <div className={styles.badge__tooltip}>{title}</div>}
        </div>
    )
}

export default Badge;