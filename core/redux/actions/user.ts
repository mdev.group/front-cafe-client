import { createAction } from "redux-actions";
import type { ICafeDTO } from "../types/dto/cafe";
import type { IUserDTO } from "../types/dto/user";

export const getUserData = createAction(
  'GET_USER_DATA'
);

export const setUserData = createAction<IUserDTO>(
  'SET_USER_DATA'
);

export const setOwnedCafes = createAction<ICafeDTO[]>(
  'SET_OWNED_CAFES'
);

export const updateUser = createAction(
  'UPDATE_USER'
);