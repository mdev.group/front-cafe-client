import { createAction } from "redux-actions";
import type { ICafeDTO } from "../types/dto/cafe";
import { IOrder } from "../types/dto/order";

export const updateOrder = createAction<Partial<IOrder>>(
  'UPDATE_ORDER'
);

export const SendOrder = createAction<Partial<IOrder>>(
  'SEND_ORDER'
);
export const successSendOrder = createAction<IOrder>(
  'SUCCESS_SEND_ORDER'
);

export const successGetOrders = createAction<IOrder[]>(
  'SUCCESS_GET_ORDERS'
);