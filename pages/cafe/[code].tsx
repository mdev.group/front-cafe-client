import { badges, categories } from "core/constants/categories";
import { beginCafeListLoad } from "core/redux/actions/cafe";
import { updateOrder } from "core/redux/actions/order";
import { cafeByCode_selector, cafes_selector, order_selector, user_selector } from "core/redux/selectors";
import { IItemDTO } from "core/redux/types/dto/item";
import { IOrderItemIngredients } from "core/redux/types/dto/order";
import Appbar from "core/uikit/Appbar/Appbar";
import Badge from "core/uikit/Badge";
import Button from "core/uikit/Button";
import FoodCategory from "core/uikit/FoodCategory";
import Grid from "core/uikit/Grid";
import Image from "core/uikit/Image";
import ItemCard from "core/uikit/ItemCard";
import { ExtendItemCard } from "core/uikit/ItemCard/ExtendItemCard";
import ModalContainer from "core/uikit/ModalContainer";
import PageHeader from "core/uikit/PageHeader";
import redirectTo from "core/utils/redirectTo";
import { uniqueId } from "lodash";
import type { NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from 'styles/pages/CafePage.module.scss'

const Home: NextPage = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { code } = router.query;
  const cafe = useSelector(cafeByCode_selector(code as string));
  const order = useSelector(order_selector);

  const [extendedItem, setExtendedItem] = useState<IItemDTO | null>(null);

  useEffect(() => {
    if (!cafe) {
      dispatch(beginCafeListLoad({
        category: null
      }));
    }
  }, [dispatch, cafe])

  const hadleChangeTake = useCallback((value) => () => {
    if (order && cafe) {
      dispatch(updateOrder({
        table: value,
        cafeID: cafe._id
      }));
    }
  }, [dispatch, order, cafe])

  const handleAddItem = useCallback((item: IItemDTO) => () => {
    if (order && cafe) {
      const ingredients: IOrderItemIngredients[] = item.composition.map((assign) => {
        const compData = item.compositionItems.find((comp) => comp._id === assign.id);

        if (compData) {
          return {
            id: assign.id,
            defaultCount: assign.isEditable ? 0 : 1,
            count: assign.isEditable ? 0 : 1,
            name: compData.name,
            price: assign.price || 0,
            isEditable: assign.isEditable,
            massStep: item.mass
          }
        }

        return undefined!;
      }).filter((data) => data !== undefined);

      dispatch(updateOrder({
        positions: [...(order.cafeID !== cafe._id ? [] : order.positions), {
          itemID: item._id,
          orderTime: Number(new Date()),
          ingredients,
          uuid: uniqueId(),
          fixed: false
        }],
        cafeID: cafe._id
      }));
    }
  }, [dispatch, order, cafe])

  const handleLongItem = useCallback((item) => () => {
    setExtendedItem(item)
  }, [])

  return (
    <div>
      {extendedItem && cafe && (
        <ModalContainer onClose={() => setExtendedItem(null)}>
          <ExtendItemCard
            item={extendedItem}
            cafe={cafe}
            onClose={() => setExtendedItem(null)}
          />
        </ModalContainer>
      )}

      <main className={styles.cafePage}>
        <Appbar />
        <PageHeader
          title={cafe?.name || 'Карточка заведения'}
          backUrl='/'
        />

        {cafe && order && (
          <>
            <div className={styles.cafePage__imageWrapper}>
              <Image
                className={styles.cafePage__image}
                src={cafe.photo_url || '/images/icons/blank/cafe.svg'}
                alt=""
              />
              <div className={styles.cafePage__badges}>
                <div>
                  {cafe.badges.map((badge, index) => (
                    <Badge
                      key={index}
                      className={styles.cafePage__badge}
                      title={badges.find((item) => item.key == badge)?.label || ''}
                    >
                      <Image
                        src={badges.find((item) => item.key == badge)?.icon || ''}
                        alt=""
                      />
                    </Badge>
                  ))}
                </div>

                <div>
                  {cafe.location && <Button
                    variant='secondary'
                    className={styles.cafePage__mapBtn}
                    onClick={() => {
                      window.open(`https://yandex.ru/maps/?pt=${cafe.location?.lng},${cafe.location?.lat}&z=18&l=map`, "_blank");
                    }}
                  >
                    <Image
                      src="/images/icons/geo.svg"
                      alt=""
                    />
                  </Button>}
                </div>
              </div>
            </div>

            <div className={styles.cafePage__container}>
              <div className={styles.cafePage__orderTake}>
                <Button variant={order.table === null ? 'primary' : 'secondary'} onClick={hadleChangeTake(null)}>С собой</Button>
                <Button variant={order.table === null ? 'secondary' : 'primary'} onClick={hadleChangeTake('0')}>В зале</Button>
              </div>

              <div className={styles.cafePage__items}>
                {cafe.items?.map((item) => (
                  <ItemCard
                    key={item._id}
                    data={item}
                    onClick={handleLongItem(item)}
                    count={order.positions.filter((ordItem) => ordItem.itemID == item._id).length}
                  />
                ))}
              </div>
            </div>
          </>

        )}

      </main>
    </div>
  );
};

export default Home;
