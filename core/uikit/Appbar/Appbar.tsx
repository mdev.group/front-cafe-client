import classNames from "classnames";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";

import styles from './Appbar.module.scss'
import { getLinks } from "./constants";
import Image from "../Image";
import { useEffect } from "react";
import { getUserData } from "core/redux/actions/user";
import { user_selector } from "core/redux/selectors";

const Appbar: React.FC = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const user = useSelector(user_selector);

  useEffect(() => {
    dispatch(getUserData());
  }, [dispatch])

  const links = getLinks(!!user);

  return (
    <div
      className={classNames(styles.appbar)}
    >
      {links.map((link, index) => (
        <Link
          key={index}
          href={link.url}
        >
          <a className={classNames(styles.appbar__link, {
            [styles['appbar__link--active']]: router.asPath.endsWith(link.url)
          })}>
            <Image
              src={link.icon}
              alt=''
            />
          </a>
        </Link>
      ))}
    </div>
  )
}

export default Appbar;