import request, { TCustomError } from './request';
import { ERequestTypes } from './api';
import { TFunc } from '../types/types';

interface IApiItem {
    type?: ERequestTypes;
    path?: (data?: any) => string;
    options?: {
        isBinary?: boolean;
    };
    requestHandler?: TFunc;
    dataHandler?: TFunc<any>;
    responseHandler?: TFunc;
}

interface TGetApiServiceItems {
    [key: string]: IApiItem;
}

type TGetApiService = (requests: TGetApiServiceItems) => any;

const getApi: TGetApiService = (requests) => {
    return Object.keys(requests).reduce((result, item) => ({
        ...result,
        [item]: async (data: any, onError: TCustomError) => {
            const { type = ERequestTypes.GET, path: getPath, options, requestHandler, dataHandler, responseHandler } = requests[item];
            const requestData = dataHandler ? (
                typeof dataHandler(data) === 'string' ? dataHandler(data).trim() : dataHandler(data)
            ) : data;

            const results = requestHandler ? requestHandler(requestData) : (await request[type](getPath!(data), requestData, {
                ...options,
                onError
            }));

            return responseHandler ? responseHandler(results) : results;
        }
    }), {});
};

export default getApi;
