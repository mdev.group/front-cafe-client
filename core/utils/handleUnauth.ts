import authTokenService from "core/services/authTokenService";
import redirectTo from "./redirectTo";

export const handleUnauth = () => {
  authTokenService.clearRefreshData();
  authTokenService.clearToken();
  redirectTo('/auth');
}