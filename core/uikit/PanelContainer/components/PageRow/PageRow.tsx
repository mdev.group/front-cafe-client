import classNames from 'classnames';
import styles from './PageRow.module.scss';

interface IProps {
  className?: string;
}

const PageRow: React.FC<IProps> = ({
  children,
  className
}) => {
  return (
    <div className={classNames(className, styles.pageRow)}>
      {children}
    </div>
  )
}

export default PageRow;