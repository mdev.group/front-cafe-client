import { useForm } from "core/hooks/useForm";
import { beginCafeUpdate } from "core/redux/actions/cafe";
import { ICafeDTO } from "core/redux/types/dto/cafe";
import Button from "core/uikit/Button";
import ImageUploader from "core/uikit/ImageUploader";
import Input from "core/uikit/Input";
import Toggle from "core/uikit/Toggle";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

import styles from './CafePanel.module.scss'

interface IProps {
  data: ICafeDTO;
}

const CafePanel: React.FC<IProps> = ({
  data
}) => {
  const dispatch = useDispatch();

  const handleUpdateCafe = (formData: any)=>{
    dispatch(beginCafeUpdate({
      id: data._id,
      newData: {
        ...formData,
        loyaltyType: formData.loyaltyType ? 2 : 0
      }
    }))

    reinit()
  };

  var {
    onChangeField,
    handleSubmit,
    formData,
    reinit
  } = useForm(handleUpdateCafe, {
    name: data.name,
    address: data.address,
    closed: data.closed,
    photo_url: data.photo_url,
    loyaltyType: !!data.loyaltyType
  });

  useEffect(() => {
    reinit()
  }, [data])

  return (
    <div className={styles.panel}>
      <div>
        <ImageUploader
          className={styles.panel__image}
          imageUrl={formData.photo_url}
          placeholderUrl={'/images/icons/blank/cafe.svg'}
          onUpload={onChangeField('photo_url')}
        />
        <div></div>
      </div>
      <div>
        <Input
          label="Название"
          value={formData['name']}
          onChange={onChangeField('name')}
          name="name"
        />
        <Input
          label="Адрес"
          value={formData['address']}
          onChange={onChangeField('address')}
          name="address"
        />

        <div className={styles.panel__row}>
          <div className={styles.panel__swithWrapper}>
            Система лояльности
            <Toggle
              value={!!formData['loyaltyType']}
              onChange={onChangeField('loyaltyType')}
            />
          </div>
          <div className={styles.panel__swithWrapper}>
            Заведение - {formData['closed'] ? 'Закрыто' : 'Открыто'}
            <Toggle
              invert
              value={formData['closed']}
              onChange={onChangeField('closed')}
            />
          </div>
        </div>

        <Button
          onClick={handleSubmit}
          size="md"
          theme="primary"
          className={styles.panel__submit}
          round
        >
          Сохранить
        </Button>
      </div>
    </div>
  )
}

export default CafePanel;