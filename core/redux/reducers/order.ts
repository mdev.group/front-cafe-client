import { createSelector } from '@reduxjs/toolkit';
import mergeDeep from 'core/utils/mergeDeep';
import { handleActions, Action } from 'redux-actions';
import { successCafeListLoad } from '../actions/cafe';
import { successGetOrders, successSendOrder, updateOrder } from '../actions/order';

import { setOwnedCafes, setUserData } from '../actions/user';

import type { ICafeDTO } from '../types/dto/cafe';
import { IEmployeeDTO } from '../types/dto/employee';
import { IComposition } from '../types/dto/item';
import { IOrder } from '../types/dto/order';
import type { IUserDTO } from '../types/dto/user';

export interface IOrderState {
  list: IOrder[];
  currentOrder: IOrder;
}

const initialState: IOrderState = {
  list: [],
  currentOrder: {
    positions: [],
    table: null
  } as any
};

// TODO: remove all any
const reducer = handleActions<IOrderState, any>({

  [updateOrder.toString()]: (state, {payload}: Action<Partial<IOrder>>) => ({
    ...state,
    currentOrder: mergeDeep(state.currentOrder, payload)
  }),

  [successSendOrder.toString()]: (state, {payload}: Action<IOrder>) => ({
    ...state,
    list: [...state.list, payload],
    currentOrder: initialState.currentOrder
  }),

  [successGetOrders.toString()]: (state, {payload}: Action<IOrder[]>) => ({
    ...state,
    list: payload
  }),

}, initialState);

export default reducer;
