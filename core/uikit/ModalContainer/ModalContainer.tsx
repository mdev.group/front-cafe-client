import { TFunc } from "core/types/types";
import { MouseEventHandler, useCallback, useEffect } from "react";
import styles from "./ModalContainer.module.scss";

interface IProps {
  onClose: TFunc;
  hideCloseIcon?: boolean;
  title?: string;
}

const Userimage: React.FC<IProps> = ({
  children,
  onClose,
  hideCloseIcon,
  title
}) => { 
  const handleClose = useCallback<MouseEventHandler>((e)=>{
    e.preventDefault();
    e.stopPropagation();
    if(e.target === e.currentTarget) {
      onClose();
    }
  }, [onClose]);

  useEffect(() => {
    window.document.body.style.overflow = 'hidden';

    return () => {
      window.document.body.style.overflow = 'auto';
    }
  }, [])

  return (
    <div className={styles.modal__wrapper} onClick={handleClose}>
      <div className={styles.modal}>
        {title && <span className={styles.modal__title}>{title}</span>}
        {!hideCloseIcon && <button onClick={handleClose} className={styles.modal__close} />}
        {children}
      </div>
    </div>
  )
}

export default Userimage;