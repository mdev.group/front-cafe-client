import classNames from 'classnames';
import { TFunc } from 'core/types/types';
import { useMemo, useState } from 'react';
import styles from './Toggle.module.scss';

interface IProps {
  invert?: boolean;
  value?: boolean;
  onChange?: TFunc;
  label?: string;
}

const Toggle: React.FC<IProps> = ({
  invert,
  value: controlledValue,
  onChange,
  label
}) => {
  const [value, setValue] = useState<any>(false);

  const fieldValue = useMemo(()=>{
    return controlledValue || value;
  }, [value, controlledValue])

  const handleChange = ()=>{
    setValue(!fieldValue);
    if(onChange) onChange(!fieldValue)
  }
  
  return (
    <button onClick={handleChange} className={classNames(styles.toggle, {
      [styles.toggle__active]: (invert && !fieldValue) || (!invert && fieldValue),
      [styles.toggle__labeled]: !!label
    })}>
      {label && <span className={styles.toggle__label}>{label}</span>}
      <div className={styles.toggle__inner}></div>
    </button>
  )
}

export default Toggle;