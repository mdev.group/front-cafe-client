import classNames from 'classnames';
import { IWorkShiftDTO } from 'core/redux/types/dto/workshift';
import { mapDayOfWeek } from 'core/utils/mapDayOfWeek';
import { useMemo } from 'react';
import styles from './WorkshiftListPanel.module.scss';

interface IProps {
  shifts: IWorkShiftDTO[];
  isActive?: (shift: IWorkShiftDTO) => boolean
  onSelectShift?: (shift: IWorkShiftDTO) => void
}

const WorkshiftListPanel: React.FC<IProps> = ({
  shifts,
  isActive,
  onSelectShift
}) => {

  const groupShifts = useMemo(() => {
    return shifts.reduce((prev, curr) => {
      const res = {...prev};
      res[curr.dayOfWeek].push(curr);
      return res;
    }, {
      1: [],
      2: [],
      3: [],
      4: [],
      5: [],
      6: [],
      7: []
    } as {[day: number]: IWorkShiftDTO[]})
  }, [shifts])

  const handleShiftSelect = (shift: IWorkShiftDTO) => () => {
    if(onSelectShift) onSelectShift(shift);
  }

  return (
    <div className={styles.panel}>
      <div className={styles.panel__hours}>
        <span>0</span>
        {[...Array(3)].map((_, index, arr) => (
          <span key={index}>
            {(index + 1) / arr.length * 24}
          </span>
        ))}
      </div>
      {Object.values(groupShifts).map((day, index) => (
        <div key={index} className={styles.panel__day}>
          <span className={styles.panel__dayLabel}>{mapDayOfWeek(index + 1)}</span>
          {day.map((shift) => {
            const top = shift.beginTime / 1440 * 100 + '%';
            const height = (shift.endTime - shift.beginTime) / 1440 * 100 + '%';

            return (
              <div
                className={classNames(styles.panel__shift, {
                  [styles['panel__shift--active']]: !isActive || isActive(shift)
                })}
                onClick={handleShiftSelect(shift)}
                key={shift._id}
                style={{
                  top,
                  height
                }}
              />
            )
          })}
        </div>
      ))}
    </div>
  )
}

export default WorkshiftListPanel;