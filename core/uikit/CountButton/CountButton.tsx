import { FunctionComponent, MouseEventHandler, ReactChild, ReactElement, useState } from "react";
import styles from './CountButton.module.scss';
import cn from 'classnames';

type SquarebuttonProps = {
  onAdd?: ()=>void;
  onSub?: ()=>void;
  value: number;
  title: string;
  subtitle?: string | number;
  className?: string;
  minCount?: number;
  maxCount?: number;
  canAdd?: boolean;
};

export const CountButton: FunctionComponent<SquarebuttonProps> = ({
  className,
  onAdd,
  onSub,
  value,
  title,
  subtitle,
  maxCount,
  minCount = 0,
  canAdd
}) => {
  const classes = cn(className, styles.CountButton, {
    [styles['CountButton--disabled']]: value !== minCount && !canAdd
  });
  
  const handleSub = ()=>{
    if(value > minCount){
      if(onSub) onSub()
    } 
  }

  const handleAdd = ()=>{
    if(typeof maxCount != 'undefined'){
      if(value < maxCount){
        if(onAdd) onAdd()
      }
    } else {
      if(onAdd) onAdd()
    }
  }

  return (
    <div className={classes}>
      <button className={styles.button} disabled={value === minCount} onClick={handleSub}>-</button>
      <div className={styles.main}>
        <div className={styles.info}>
          <span className={styles.title}>{title}</span>
          {subtitle && <span className={styles.subtitle}>{subtitle}</span>}
        </div>
        <div className={styles.count}>
          <span>x{value}</span>
        </div>
      </div>
      <button className={styles.button} disabled={value === maxCount} onClick={handleAdd}>+</button>
    </div>
  );
};
