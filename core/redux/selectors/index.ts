import { createSelector } from "@reduxjs/toolkit"
import { IRootReducer } from "../reducers"
import { ICafeDTO } from "../types/dto/cafe";
import { IOrder } from "../types/dto/order";

export const user_selector = (state: IRootReducer) => state.user.userData;

export const cafes_selector = (state: IRootReducer) => state.cafe.list;
export const order_selector = (state: IRootReducer) => state.order.currentOrder;
export const ordersList_selector = (state: IRootReducer) => state.order.list;

export const cafeByCode_selector = (code: string) => createSelector([cafes_selector], (cafes) => cafes?.find(item => item.code === code) || null)
export const cafeByID_selector = (id: string) => createSelector([cafes_selector], (cafes) => cafes?.find(item => item._id === id) || null)

export const ordersListTotals_selector = createSelector([ordersList_selector, cafes_selector], (orders, cafes) => {
  if(orders && cafes) {
    return orders.map((order) => {
      return getOrderTotal(order, cafes);
    })
  }

  return null;
})



export const orderCartTotal_selector = createSelector([order_selector, cafes_selector], (order, cafes) => {
  if(cafes && order) {
    return getOrderTotal(order, cafes);
  }
  return null;
})


const getOrderTotal = (order: IOrder, cafes: ICafeDTO[]) => {
  const cafe = cafes?.find((item) => item._id === order.cafeID)

  const cost = order.positions.reduce((prev, position) => {
    const item = cafe?.items.find((item) => position.itemID === item._id)

    let baseCost = 0;
    let additiveCost = 0;
    if(item) {
      baseCost = item.price || 0
      additiveCost = position.ingredients.reduce((prev, ing) => {
        const count = Math.max(0, ing.count - ing.defaultCount);
        const price = count*ing.price;
        return prev + price;
      }, 0);
    }
    const finCost = baseCost + additiveCost;

    return prev + finCost;
  }, 0) || 0


  return {
    cost,
    orderID: order._id,
    cafeID: cafe?._id
  }
}
