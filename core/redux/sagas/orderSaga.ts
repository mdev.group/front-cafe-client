import delay from '@redux-saga/delay-p'
import { takeEvery, put, call, select } from 'redux-saga/effects'
import api from '../../api';


import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import { beginCafeListLoad, successCafeListLoad } from '../actions/cafe';
import { ICafeDTO } from '../types/dto/cafe';
import { SendOrder, successGetOrders, successSendOrder, updateOrder } from '../actions/order';
import { IOrder } from '../types/dto/order';
import { Action } from 'redux-actions';
import redirectTo from 'core/utils/redirectTo';
import { getUserData } from '../actions/user';
import { order_selector, user_selector } from '../selectors';
import { IUserDTO } from '../types/dto/user';
import mergeDeep from 'core/utils/mergeDeep';

function* send({payload}: Action<Partial<IOrder>>): Generator {
  try {
    const response = (yield call(api.sendOrder, payload)) as IOrder;
    yield put(successSendOrder(response));

    localStorage.removeItem('cart')
    
    redirectTo(`/orders/${response._id}`)
  } catch (error) {
      yield put(requestError(error));
  }
}

function* getOrders(): Generator {
  try {
    yield call(waitForAuth);
    const user = (yield select(user_selector)) as IUserDTO;

    const response = (yield call(api.getOrders)) as IOrder[];
    yield put(successGetOrders(response));
  } catch (error) {
      yield put(requestError('order update error'));
  }
}

function* saveCart({payload}: Action<any>): Generator {
  try {
    const cart = (yield select(order_selector)) as IUserDTO;
    localStorage.setItem('cart', JSON.stringify(mergeDeep(cart, payload)))
  } catch (error) {
      yield put(requestError('order update error'));
  }
}

function* loadCart(): Generator {
  try {
    const cartDataJSON = localStorage.getItem('cart');
    if(cartDataJSON) {
      const cartData = JSON.parse(cartDataJSON)

      yield put(updateOrder(cartData));
    }
  } catch (error) {
      yield put(requestError('order update error'));
  }
}


function* fetchSagaPeriodically() {
  yield call(waitForAuth);

  console.log('bind orders check')
  while (true) {
    yield call(getOrders);
    yield delay(30000);
  }
}

export function* initOrderWatchers(){
  yield takeEvery(SendOrder.toString(), send);
  yield takeEvery(updateOrder.toString(), saveCart);

  yield call(loadCart)
  yield call(fetchSagaPeriodically)
}