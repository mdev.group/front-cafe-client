import { createAction } from "redux-actions";
import type { ICafeDTO } from "../types/dto/cafe";

export interface beginCafeListLoad__payload {
  category: string | null;
  ids?: string[];
}
export const beginCafeListLoad = createAction<beginCafeListLoad__payload>(
  'BEGIN_CAFE_LIST_LOAD'
);

export const successCafeListLoad = createAction<ICafeDTO[]>(
  'SUCCESS_CAFE_LIST_LOAD'
);