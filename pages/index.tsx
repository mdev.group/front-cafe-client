import { categories } from "core/constants/categories";
import { beginCafeListLoad } from "core/redux/actions/cafe";
import { cafes_selector, user_selector } from "core/redux/selectors";
import Appbar from "core/uikit/Appbar/Appbar";
import CafeCard from "core/uikit/CafeCard";
import FoodCategory from "core/uikit/FoodCategory";
import Input from "core/uikit/Input";
import PageHeader from "core/uikit/PageHeader";
import { getDistanceFromLatLonInKm } from "core/utils/getDistanceFromLatLonInKm";
import redirectTo from "core/utils/redirectTo";
import type { NextPage } from "next";
import Head from "next/head";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useCurrentPosition } from 'react-use-geolocation';

import styles from 'styles/pages/CatalogPage.module.scss'

const Home: NextPage = () => {
  const dispatch = useDispatch();
  const cafesList = useSelector(cafes_selector);

  const [searchString, setSearchString] = useState('');
  const [selectedCat, selectCat] = useState<string | null>(null);

  const handleUpdateCategory = useCallback((cat) => () => {
    if(cat == selectedCat) {
      selectCat(null)
      return ;
    }

    selectCat(cat)
  }, [selectedCat])

  useEffect(() => {
    dispatch(beginCafeListLoad({
      category: selectedCat
    }));
  }, [dispatch])
  
  const [position, error] = useCurrentPosition();

  const filteredCafes = useMemo(() => {
    return (cafesList || [])
      .filter((cafe) => !cafe.closed)
      .filter((cafe) => selectedCat == null || cafe.categories.includes(selectedCat))
      .filter((cafe) => cafe.name.includes(searchString) || searchString.length < 3)
      .sort((a, b) => {
        if(!position) {
          return 0;
        }
        const {latitude, longitude} = position.coords
        const AdistanceInM = getDistanceFromLatLonInKm(latitude, longitude, Number(a.location?.lat), Number(a.location?.lng)) * 1000;
        const BdistanceInM = getDistanceFromLatLonInKm(latitude, longitude, Number(b.location?.lat), Number(b.location?.lng)) * 1000;

        return AdistanceInM - BdistanceInM;
      });
  }, [selectedCat, cafesList, searchString, position])

  return (
    <div>
      <main>
        <Appbar />
        <PageHeader title="Поиск заведения" />

        <div className={styles.page__categories}>
          {categories.map((cat, index) => (
            <FoodCategory
              key={index}
              active={selectedCat === cat.label}
              label={cat.label}
              icon={cat.icon}
              color={cat.color}
              onClick={handleUpdateCategory(cat.label)}
            />
          ))}
        </div>

        <div className={styles.page__search}>
          <Input
            onChange={(value) => setSearchString(value)}
            value={searchString}
            className={styles.page__searchInput}
            placeholder="Поиск по названию..."
          />
        </div>

        <div className={styles.page__cafes}>
          {filteredCafes?.map((cafe) => (
            <CafeCard
              key={cafe._id}
              data={cafe}
              onClick={() => redirectTo(`/cafe/${cafe.code}`)}
            />
          ))}
        </div>

      </main>
    </div>
  );
};

export default Home;
