import { createSelector } from '@reduxjs/toolkit';
import { handleActions, Action } from 'redux-actions';

import { setOwnedCafes, setUserData } from '../actions/user';

import type { ICafeDTO } from '../types/dto/cafe';
import { IClient } from '../types/dto/client';
import { IEmployeeDTO } from '../types/dto/employee';
import { IComposition } from '../types/dto/item';
import type { IUserDTO } from '../types/dto/user';

export interface IUsertate {
  userData?: IClient;
  ownedCafes?: ICafeDTO[];
  selectedCafeID?: string | null;
}

const initialState: IUsertate = {
  userData: undefined,
  ownedCafes: undefined,
  selectedCafeID: typeof window !== 'undefined' ? window.localStorage?.getItem('cafeID') || undefined : undefined
};

// TODO: remove all any
const reducer = handleActions<IUsertate, any>({
  [setUserData.toString()]: (state, {payload}: Action<IClient>) => ({
    ...state,
    userData: payload
  }),
  
  [setOwnedCafes.toString()]: (state, {payload}: Action<ICafeDTO[]>) => ({
    ...state,
    ownedCafes: payload
  }),

}, initialState);

export default reducer;
