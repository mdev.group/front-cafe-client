import * as crypto from 'crypto';

function appendHash(payload: any) {
  if(process.env.NEXT_PUBLIC_DEV_TG_SECRET) {
    const secret = crypto.createHash('sha256').update(process.env.NEXT_PUBLIC_DEV_TG_SECRET).digest();

    const check = crypto.createHmac('sha256', secret).update(
      Object
      .keys(payload)
      .map((key) => `${key}=${payload[key]}`)
      .sort()
      .join('\n')
    ).digest('hex');
    
    return {
      ...payload,
      hash: check
    };
  } 
}

export const mockData = appendHash({
  auth_date: Math.ceil(Number(new Date()) / 1000),
  first_name: "Антон",
  id: Number(process.env.NEXT_PUBLIC_DEV_TG_AUTH_ID),
  last_name: "Медведев",
  photo_url: "https://t.me/i/userpic/320/1QiBk4x-s0qIQcEBZicDNNGYCPQXhXpLA56G2OGmJis.jpg",
  username: "iamtoxa"
})