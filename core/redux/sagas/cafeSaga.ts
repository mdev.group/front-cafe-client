import { takeEvery, put, call } from 'redux-saga/effects'
import api from '../../api';


import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import { beginCafeListLoad, successCafeListLoad } from '../actions/cafe';
import { ICafeDTO } from '../types/dto/cafe';

function* loadList(): Generator {
  try {
    const response = (yield call(api.getCafeList)) as ICafeDTO[];
    yield put(successCafeListLoad(response));
  } catch (error) {
      yield put(requestError(error));
  }
}


export function* initCafeWatchers(){
  yield takeEvery(beginCafeListLoad.toString(), loadList);
}