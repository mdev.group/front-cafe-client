export interface IPFC {
  proteins: number; //x4
  fats: number; //x9
  carbohydrates: number; //x4
}

export interface IComposition {
  _id: string;
  creatorID: string;
  name: string;
  pfc?: IPFC;
}

export interface ICompositionAssign {
  id: string;
  mass: number;
  isEditable: boolean;
  price: number;
}

export interface IItemDTO {
  name: string;
  mass: number;
  photo_url: string;
  composition: ICompositionAssign[];
  compositionItems: IComposition[];
  cafeID: string;
  price: number;
  _id: string;
}