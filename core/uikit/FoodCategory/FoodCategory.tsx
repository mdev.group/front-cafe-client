import classNames from "classnames";
import { TFunc } from "core/types/types";
import { CSSProperties } from "react";
import Image from "../Image";

import styles from './FoodCategory.module.scss'

interface IProps {
  active?: boolean;
  label?: string;
  onClick?: TFunc;
  icon?: string;
  color?: string;
  extended?: boolean;
}

const FoodCategory: React.FC<IProps> = ({
  active,
  label,
  onClick,
  icon,
  color,
  extended
}) => {
  return (
    <div
      className={classNames(styles.category, {
        [styles['category--active']]: active,
        [styles['category--extended']]: extended
      })}
      onClick={onClick}
      style={{
        '--color': color
      } as CSSProperties}
    >
      <div className={styles.category__container}>
        <Image
          className={styles.category__icon}
          src={icon || "/images/Food/pizza.png"}
          alt=""
        />

        <span className={styles.category__label}>
          {label}
        </span>
      </div>
    </div>
  )
};

export default FoodCategory;