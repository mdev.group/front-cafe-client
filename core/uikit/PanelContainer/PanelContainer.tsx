import Image from "core/uikit/Image";
import styles from "./PanelContainer.module.scss";

interface IProps {
  loading: boolean;
}

const Userimage: React.FC<IProps> = ({
  children,
  loading
}) => { 
  return (
    <main className={styles.panelcontainer}>
      {loading ? <Image className={styles.panelcontainer__loading} src="/images/icons/loading.svg" alt=""/> : children}
    </main>
  )
}

export default Userimage;