import { order_selector } from 'core/redux/selectors';
import redirectTo from 'core/utils/redirectTo';
import Head from 'next/head';
import { useCallback } from 'react';
import { useSelector } from 'react-redux';
import Button from '../Button';
import Image from '../Image';
import styles from './PageHeader.module.scss';

interface IProps {
  title: string;
  backUrl?: string;
}

const PageHeader: React.FC<IProps> = ({
  title,
  backUrl
}) => {

  const order = useSelector(order_selector);

  const handleBack = useCallback(() => {
    if(backUrl) redirectTo(backUrl);
  }, [backUrl])

  return (
    <div className={styles.header}>
      <Head>
        <title>MD.Resto | {title}</title>
      </Head>

      <div className={styles.header__group}>
        {backUrl && (
          <Button variant='transparent' className={styles.header__back} onClick={handleBack}>
            <Image alt='back' src='/images/icons/chevron-bottom.svg' />
          </Button>
        )}
        <h1 className={styles.header__title}>
          {title}
        </h1>
      </div>

      <div>
        <Button variant='transparent' className={styles.header__cart} onClick={() => redirectTo('/cart')}>
          <Image alt='back' src='/images/icons/cart.svg' />
          {order.positions.length > 0 && <div className={styles.header__cartBadge}>{order.positions.length}</div>}
        </Button>
      </div>
    </div>
  );
};

export default PageHeader;