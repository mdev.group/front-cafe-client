import classNames from "classnames";

import styles from './PositionCard.module.scss';
import { IOrder, IOrderPosition } from "core/redux/types/dto/order";
import Image from "../Image";
import { useDispatch, useSelector } from "react-redux";
import { cafeByID_selector, order_selector } from "core/redux/selectors";
import { useCallback, useMemo } from "react";
import { updateOrder } from "core/redux/actions/order";

interface IProps {
  position: IOrderPosition;
  order: IOrder;
}

const PositionCard: React.FC<IProps> = ({
  position,
  order
}) => {
  const dispatch = useDispatch();
  const cafe = useSelector(cafeByID_selector(order.cafeID));

  const item = useMemo(() => {
    if (cafe) {
      return cafe.items.find((item) => item._id === position.itemID) || null!
    }
    return null!
  }, [cafe, position])

  const handleDel = useCallback(() => {
    dispatch(updateOrder({
      positions: order.positions.filter((item) => item.uuid !== position.uuid)
    }))
  }, [position, order, dispatch])

  const diffIngredients = useMemo(() => {
    return position.ingredients.filter((ing) => {
      return ing.count !== ing.defaultCount;
    })
  }, [position])

  return (
    <div
      className={classNames(styles.card)}
    >
      {item && (
        <>
          <button onClick={handleDel} className={styles.card__close} />
          <div className={styles.card__mainInfo}>
            <Image
              src={item.photo_url}
              alt=""
              className={styles.card__photo}
            />
            <div className={styles.card__mainInfoTitles}>
              <span className={styles.card__name}>{item.name}</span>
              <span className={styles.card__mass}>{item.mass} гр.</span>

              <span className={styles.card__price}>{item.price} руб.</span>
            </div>
          </div>

          {diffIngredients.length > 0 && (
            <div className={styles.card__ings}>
              {diffIngredients.map((ing) => (
                <div key={ing.id} className={classNames(styles.card__ing, {
                  [styles['card__ing--add']]: ing.count > 0,
                  [styles['card__ing--sub']]: ing.count == 0
                })}>
                  <span>
                    {ing.count > 0 ? `x${ing.count}` : '-'}
                  </span>
                  <span>{ing.name}</span>
                  {ing.count > 0 && <span>
                    (+ {ing.price * (ing.count - ing.defaultCount)} руб.)
                  </span>}
                </div>
              ))}
            </div>
          )}
        </>
      )}
    </div>
  )
}

export default PositionCard;