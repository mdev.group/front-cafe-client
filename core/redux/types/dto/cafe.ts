import type { IEmployeeDTO } from "./employee";
import { IItemDTO } from "./item";
import type { IWorkShiftDTO } from "./workshift";

export enum ELoyaltyType {
  DISABLED,
  LOCAL,
  GLOBAL,
}

interface ILocation {
  lat: string;
  lng: string;
}

export interface ICafeDTO {
  _id: string;
  secret: string;
  ownerID: string;
  name: string;
  address: string;
  creationDate: number;
  closed: boolean;
  loyaltyType: ELoyaltyType;
  employees: IEmployeeDTO[];
  workshifts: IWorkShiftDTO[];
  items: IItemDTO[]
  photo_url?: string;
  categories: string[];
  badges: string[];
  acceptOnlineOrders: boolean;
  location?: ILocation;
  code?: string;
}

export interface IUpdateCafeDTO {
  name?: string;
  image?: string;
  loyaltyType?: ELoyaltyType;
  closed?: boolean;
}