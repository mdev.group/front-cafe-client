export const getLinks = (isAuthed: boolean) => [
  {
    url: '/',
    icon: '/images/icons/nav/home.svg'
  },
  {
    url: '/favorites',
    icon: '/images/icons/nav/like.svg'
  },
  {
    url: isAuthed ? '/profile' : '/auth',
    icon: !isAuthed ? '/images/icons/sign-in.svg' : '/images/icons/nav/profile.svg'
  }
]