import classNames from "classnames";
import { ChangeEventHandler, useCallback, useState } from "react";
import Dropdown from "../Dropdown";
import Input from "../Input/Input";

import styles from './SearchInput.module.scss';

interface IItem {
  label: string;
  onClick: () => void;
}

interface IProps {
  items: IItem[];
  placeholder?: string;
  label?: string;
  onUpdateSearch?: (value: string) => void
  className?: string;
  value?: string;
  onChange?: (value: string) => void;
}

const SearchInput: React.FC<IProps> = ({
  label,
  onUpdateSearch,
  className,
  items,
  value: valueLabel,
  onChange
}) => {
  const [value, setValue] = useState('');

  const handleChange = useCallback((newValue) => {
    setValue(newValue);
    if(onChange) onChange(newValue);

    if(newValue.length >= 3) {
      if(onUpdateSearch) {
        onUpdateSearch(newValue)
      }
    }
  }, [onChange, onUpdateSearch]);

  const handleClick = useCallback((item: IItem) => () => {
    if(item.onClick) item.onClick()
    setValue('')
  }, [])

  return (
    <div className={classNames(className, styles.field)}>
      <Input
        value={valueLabel || value}
        onChange={handleChange}
        label={label}
        className={className}
      />
      
      {value.length >= 3 && items && items.length > 0 && (
        <Dropdown
          classNames={{
            main: styles.field__dropdown
          }}
          items={items.map((item) => (
            {
              onClick: handleClick(item),
              label: item.label
            }
          ))}
          onClose={() => {}}
        />
      )}
    </div>
  )
};

export default SearchInput;