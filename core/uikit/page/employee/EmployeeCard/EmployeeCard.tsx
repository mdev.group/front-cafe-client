import type { IEmployeeDTO } from "core/redux/types/dto/employee";
import type { TFunc } from "core/types/types";
import Badge from "../../../Badge";
import Image from "core/uikit/Image";

import styles from './EmployeeCard.module.scss'
import { formatPhone, formatPhoneNumber } from "core/utils/phone";
import { useCallback, useEffect, useMemo, useState } from "react";
import classNames from "classnames";
import Moment from "react-moment";
import Button from "core/uikit/Button";
import { useForm } from "core/hooks/useForm";
import Input from "core/uikit/Input";
import { useDispatch } from "react-redux";
import { beginEmployeeUpdate } from "core/redux/actions/cafe";
import ImageUploader from "core/uikit/ImageUploader";

interface IProps {
    employeeInfo: IEmployeeDTO;
    onClick?: TFunc;
    extended?: boolean;
    appendBadge?: string;
}

const EmployeeCard: React.FC<IProps> = ({
    employeeInfo,
    onClick,
    extended,
    appendBadge
}) => {
    const dispatch = useDispatch();
    const [edit, setEdit] = useState(false);
    
    const handleUpdateCafe = (data: any) => {
        console.log(data);
        dispatch(beginEmployeeUpdate({
            id: employeeInfo._id,
            newData: {
                first_name: data.first_name,
                last_name: data.last_name,
                position: data.position,
                photo_url: data.photo_url
            }
        }))
    }

    const handleFire = useCallback(() => {
        dispatch(beginEmployeeUpdate({
            id: employeeInfo._id,
            newData: {
                fireDate: employeeInfo.fireDate ? null : Number(new Date()) / 1000
            }
        }))
    }, [dispatch, employeeInfo._id, employeeInfo.fireDate])

    const {
        onChangeField,
        handleSubmit,
        formData,
        reinit
    } = useForm(handleUpdateCafe, {
        ...employeeInfo
    });

    const handleEdit = useCallback(() => {
        if (edit) {
            setEdit(false)
            handleSubmit();
        } else {
            setEdit(true)
        }
    }, [edit, handleSubmit])

    useEffect(() => {
        reinit()
    }, [employeeInfo])

    return (
        <div className={classNames(styles.employeeCard, {
            [styles['employeeCard--extended']]: extended,
            [styles['employeeCard--pointer']]: !!onClick,
            [styles['employeeCard--fired']]: !!employeeInfo.fireDate
        })} onClick={onClick}>
            <div className={styles.employeeCard__additive}>
                {extended ? (
                    <>
                        <Button theme={employeeInfo.fireDate ? 'primary' : 'danger'} size='sm' round variant='outline' onClick={handleFire}>
                            {employeeInfo.fireDate ? 'Нанять' : 'Уволить'}
                        </Button>

                        <Button theme='primary' size='sm' round variant='outline' onClick={handleEdit}>
                            {edit ? 'Сохранить' : 'Редактировать'}
                        </Button>
                    </>
                ) : (
                    <>
                        <Badge>
                            {employeeInfo.position}
                        </Badge>
                        {appendBadge && (
                            <Badge>
                                {appendBadge}
                            </Badge> 
                        )}
                    </>
                )}
            </div>
            {edit ? (
                <ImageUploader
                    className={styles.employeeCard__image}
                    imageUrl={formData.photo_url}
                    placeholderUrl={'/images/icons/blank/user.svg'}
                    onUpload={onChangeField('photo_url')}
                />
            ) : (
                <Image
                    className={styles.employeeCard__image}
                    src={formData.photo_url}
                    placeholderUrl={'/images/icons/blank/user.svg'}
                    alt=""
                />
            )}
            <div className={styles.employeeCard__info}>
                {!edit && <span className={styles.employeeCard__code}>#{employeeInfo.secret}</span>}
                
                {edit ? (
                    <div className={styles.employeeCard__inputRow}>
                        <Input
                            label="Имя"
                            value={formData['first_name']}
                            onChange={onChangeField('first_name')}
                            name="first_name"
                        />
                        <Input
                            label="Фамилия"
                            value={formData['last_name']}
                            onChange={onChangeField('last_name')}
                            name="last_name"
                        />
                    </div>
                ) : (
                    <span className={styles.employeeCard__name}>{`${employeeInfo.first_name} ${employeeInfo.last_name}`}</span>
                )}

                {extended && (
                    <>
                        {edit ? (
                            <>
                                <Input
                                    className={styles.employeeCard__positionInput}
                                    label="Должность"
                                    value={formData['position']}
                                    onChange={onChangeField('position')}
                                    name="position"
                                />
                            </>
                        ) : (
                            <>
                                <span className={styles.employeeCard__text}>Должность: {employeeInfo.position || '-'}</span>
                                <span className={styles.employeeCard__text}>День найма: <Moment format="DD.MM.YYYY">{employeeInfo.hireDate * 1000}</Moment></span>
                                <span className={styles.employeeCard__text}>Дата увольнения:{' '}
                                    {employeeInfo.fireDate ? <Moment format="DD.MM.YYYY">{employeeInfo.fireDate * 1000}</Moment> : '-'}
                                </span>
                            </>
                        )}
                    </>
                )}
            </div>
        </div>
    )
}

export default EmployeeCard;